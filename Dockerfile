FROM python:3.8.0b1-alpine3.9

LABEL MAINTAINER='TBD'

ENV PYTHONUNBUFFERED 1

ADD . /app
WORKDIR /app
COPY requirements.txt ./

RUN apk update &&\
    apk add postgresql-dev gcc python3-dev musl-dev

# Add build deps for pgsql 
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000
CMD python3 manage.py makemigrations && python3 manage.py migrate && python3 manage.py runserver 0.0.0.0:8000