from django.urls import include, path

from .example_view import ExampleView

urlpatterns = [
    path('', ExampleView.as_view()),
]
