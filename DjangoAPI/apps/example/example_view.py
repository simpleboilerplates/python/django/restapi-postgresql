from rest_framework.response import Response
from rest_framework.views import APIView


class ExampleView(APIView):
    """
    An example ViewSet. You can copy paste this app as you wish.
    """

    def get(self, request, format=None):
        return Response('Hello World')
